function map(elements, cb) {
  if (Array.isArray(elements) && typeof cb === "function") {
    let newArr = [];
    for (let i = 0; i < elements.length; i++) {
      newArr.push(cb(elements[i], i, elements));
    }
    return newArr;
  } else {
    console.log(
      "First argument should be an array and second argument should be a function"
    );
    return [];
  }
}
function cb(element, index, array) {
  return 2 * element;
}

module.exports = { map, cb };
