function flatten(elements, flattenArray) {
  if (Array.isArray(elements)) {
    for (let i = 0; i < elements.length; i++) {
      if (Array.isArray(elements[i])) {
        flatten(elements[i], flattenArray);
      } else {
        flattenArray.push(elements[i]);
      }
    }
  } else {
    console.log("First argument should be an array");
    return [];
  }
}

module.exports = flatten;
