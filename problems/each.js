function each(elements, cb) {
  if (Array.isArray(elements) && typeof cb === "function") {
    for (let i = 0; i < elements.length; i++) {
      cb(elements[i], i, elements);
    }
  } else {
    console.log(
      "First argument should be an array and second argument should be a function"
    );
  }
}
function cb(element, index, array) {
  console.log(element);
}

module.exports = { each, cb };
