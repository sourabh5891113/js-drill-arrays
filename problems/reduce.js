function reduce(elements, cb, startingValue) {
  if (Array.isArray(elements) && typeof cb === "function") {
    let accumulator = startingValue === undefined ? elements[0] : startingValue;
    let startIndex = startingValue === undefined ? 1 : 0;

    for (let i = startIndex; i < elements.length; i++) {
      accumulator = cb(accumulator, elements[i], i, elements);
    }
    return accumulator;
  } else {
    console.log(
      "First argument should be an array and second argument should be a function"
    );
    return 0;
  }
}
function cb(acc, element, index, array) {
  return acc + element;
}

module.exports = { reduce, cb };
