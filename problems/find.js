function find(elements, cb) {
  if (Array.isArray(elements) && typeof cb === "function") {
    for (let i = 0; i < elements.length; i++) {
      if (cb(elements[i], i, elements)) {
        return elements[i];
      }
    }
  } else {
    console.log(
      "First argument should be an array and second argument should be a function"
    );
  }
}
function cb(element, index, array) {
  return element > 3;
}

module.exports = { find, cb };
