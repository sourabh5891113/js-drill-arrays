function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
  if (Array.isArray(elements) && typeof cb === "function") {
    let filteredArray = [];
    for (let i = 0; i < elements.length; i++) {
      if (cb(elements[i], i, elements)) {
        filteredArray.push(elements[i]);
      }
    }
    return filteredArray;
  } else {
    console.log(
      "First argument should be an array and second argument should be a function"
    );
    return [];
  }
}

function cb(element, index, array) {
  return element % 2 === 0;
}

module.exports = { filter, cb };
